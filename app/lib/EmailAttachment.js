// Module Info
exports.name = "EmailAttachment.js";
exports.version = "1.0";



/* 
  Generate PDF File
  @argument: html - The HTML content for the PDF
             filename - The name of the file to create
*/ 
exports.generatePDF = function(html, filename="temp.pdf"){
  var NappPDFCreator = require('dk.napp.pdf.creator');
  var minified = html.replace(/\n|\t/g, ' ');

  return new Promise((resolve, reject) => {
      var re = NappPDFCreator.generatePDFWithHTML({
        html : minified,
        filename : filename,
        letter: false
      });

      NappPDFCreator.addEventListener("complete", function(e){
        Ti.API.info("COMPLETE");
        var file = Ti.Filesystem.getFile(e.path);
        resolve({success: true, file: file, path: e.path});
      });

      NappPDFCreator.addEventListener("error", function(e){
        Ti.API.error(e);
        reject({error: e.error, type: 'pdf'})
      });
  });
}



/*
  Generate CSV File
  @argument: objArr - A Key:Value object array representing the names and values for the CSV
             filename - The name of the file to create
*/
exports.generateCSV = function(objArr, filename="temp.csv"){
  Ti.API.info("#### generateCSV ####");
  Ti.API.info(objArr);
  var content = csvStringFromObject(objArr);
  Ti.API.info(content);
  var csv = Titanium.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory, filename);

  return new Promise((resolve, reject) => {
    if(csv.exists() && csv.isFile()){
      if( csv.deleteFile() ){
        csv.createFile();
        csv.write(content,false);
      }
    }
    else{
      csv.createFile();
      csv.write(content, false);
    }

    resolve({success: true, file: csv, path: csv.resolve()})
  });
}

function csvStringFromObject(oa) {
  var lineArray = [];

  // Make sure oa is an array of values
  if ( !Array.isArray(oa) && typeof(oa) == 'object' ) {
    oa = [oa];
  }

  // Headings
  var keys = Object.keys(oa[0]);

  // Removce Spaces and put in Title Case
  var formattedKeys = keys.map(k => { 
    var spaced = k.split("_").join(" ");
    return spaced.replace(/\b(\w)/g, s => s.toUpperCase());
  });
  lineArray.push(formattedKeys.join(','));

  // Values
  oa.forEach((obj) => {
    var vals = Object.values(obj);
    lineArray.push(vals.join(','));
  });

  return lineArray.join('\n');
}
var URL = (Ti.App.delpoyType === 'production') ? 'http://backend.productworld.com' : 'http://backend.staging.productworld.com',
	companyID = '';

function SemanticSearch(company){
	companyID = company;
}

// ###########################################################################

function httpClient(url,onsuccess,onerror){

	return new Promise((resolve, reject) => {
		var client = Ti.Network.createHTTPClient({
		    onload : function(e) {
		    	resolve({code: this.status, response: this.responseText});
		    },
		    onerror : function(e) {
		    	reject({code: this.status, error: e.error});
		    },
		    timeout : 5000
		});
		client.open("GET", url);
		client.setRequestHeader("User-Agent",Ti.App.name+" "+Ti.App.version);
		client.send();
	});
}

// ###########################################################################

SemanticSearch.prototype.getEmployeeData = function(){
	var uri = URL + '/api/search/?limit=100&category=5abcc0362e1fa976ba19f7a4';
	return httpClient(uri);
};

// ###########################################################################

SemanticSearch.prototype.getICBData = function(onsuccess,onerror){
	var uri = URL + '/api/search/?limit=300&category=5abcbbc22e1fa976ba19f7a1';
	return httpClient(uri);
};

// ###########################################################################

SemanticSearch.prototype.getLocation = function(onsuccess,onerror){
	var uri = URL + '/api/search/?limit=300&category=5abcc0272e1fa9078619f7a5';
	return httpClient(uri);
};

// ###########################################################################

module.exports = SemanticSearch;

// Module Info
exports.name = "HTMLReports.js";
exports.version = "1.0";

// Defaults
var _details = {type: "SAFETY", content:{
	name: "Johann Von Schmidt",
	id: "01123581321",
	num_observed: "1",
	date: "May 17, 2018",
	location: "Warehouse",
	department: "Handling",
	safe: false,
	safe_value: "&nbsp;",
	risk: true,
	risk_value: "&check;",
	action_observed: "Using incorrect technique while lifting boxes from an elevated point.",
	consequences: "Injury to himself or others.",
	action_taken: "Instructed Johann on correct lifting techniques and practices."
}};

// Inner Tempates
var templates = {
	isCheckmark: (bool) => {
		return bool ? "&check;": "&nbsp;";
	},
	combine: (data, templateString) => {
		return new Function("return `"+templateString+"`;").call(data);
	}
}

var urls = {
	SAFETY: "https://bitbucket.org/shilei_liu/dnata/raw/master/templates/safety.html?"+(new Date().getTime()),
	BAPP: "https://bitbucket.org/shilei_liu/dnata/raw/master/templates/bapp.html?"+(new Date().getTime()),
	CHECKLIST: "https://bitbucket.org/shilei_liu/dnata/raw/master/templates/bapp_checklist.html?"+(new Date().getTime()),
}

//Methods
exports.generateReport = function (details) {
	details = details || _details;
	var url = urls[details.type];

	return new Promise((resolve, reject) => {
		var client = Ti.Network.createHTTPClient({
		    onload : function(e) {
		    	var html = "";
		    	if (details.type == "CHECKLIST") {
		    		// Modify content
		    		var content = modifyChecklistContent(details.content);
		    		html = templates.combine(content, this.responseText);
		    	}else{
		    		html = templates.combine(details.content, this.responseText);
		    	}
		        resolve({code: this.status, response: html});
		    },
		    onerror : function(e) {
		        reject({code: this.status, error: e.error, type: 'html'});
		    },
		    timeout : 5000
		});
		client.open("GET", url);
		client.send();
	});
}

function listHTML(item) {
	let safe_value = item.safe ? '&check;' : '&nbsp;';
	let risk_value = item.risk ? '&check;' : '&nbsp;';

	return `
	<div class='item'>
        <div class='name'>${item.title}</div>
        <div class='safe'><span>${safe_value}</span></div>
        <div class='risk'><span>${risk_value}</span></div>
    </div>`;
}

function createColumnsForSections( sections ) {
	let oddSections = sections.filter((element, index, array) => { return index%2 === 1; });
	let evenSections = sections.filter((element, index, array) => { return index%2 === 0; });

	return `
		<div class="column">
        	<div class="column-header">
          		<div class="name"></div>
          		<div class="safe"><span>SAFE</span></div>
          		<div class="risk"><span>AT-RISK</span></div>
        	</div>
        	${evenSections.join('\n')}
      	</div>
      	<div class="column">
        	<div class="column-header">
          		<div class="name"></div>
          		<div class="safe"><span>SAFE</span></div>
          		<div class="risk"><span>AT-RISK</span></div>
        	</div>
        	${oddSections.join('\n')}
      	</div>
	`;
}

function modifyChecklistContent( content ) {

	let categoryList = {};
	content.forEach(item => {
		if(!categoryList.hasOwnProperty(item.category)){
      		categoryList[item.category] = [item];
    	}else{
    		categoryList[item.category].push(item);
    	}
	});

	let sections = [];
	for (let key in categoryList){
		
		sections.push(`
		<div class='section'>
			<div class='header'>
				<p>${key}</p>
			</div>
	  		${categoryList[key].map(listHTML).join('\n')}
		</div>
		`);
	};
	
	let html_output = createColumnsForSections(sections);
	return {categorylist: html_output};
}

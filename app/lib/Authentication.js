let EmployeeData;

function loadingEmployeeData(){
  let SemanticSearch = require('SemanticSearch');
  let dnata = new SemanticSearch('dnata');
  if(EmployeeData === undefined){
    dnata.getEmployeeData().then((result) => {
      EmployeeData = JSON.parse(result.response);
      return EmployeeData; 
    });
  }else{
    return EmployeeData;
  }
}
exports.loadingEmployeeData = loadingEmployeeData;

exports.login = function(type){ 

  return new Promise((resolve, reject) => {
    checkAuth(type).then(function(result){
      Ti.API.info(result);
      if(result.success){
        resolve(result)
      }else{
        Ti.API.info(result);
        loginDialog(type).then(function(result2){
          Ti.API.info(result2);
          resolve(result2); 
        });
      }
    }) 
  })
}

function loginDialog(type){

  return new Promise((resolve,reject) => {
    Ti.API.info('loginDialog='+type);
    let dialog = Ti.UI.createAlertDialog({
        title: 'Login',
        style: Ti.UI.iOS.AlertDialogStyle.LOGIN_AND_PASSWORD_INPUT ,
        loginHintText: 'Username',
        passwordHintText: 'Employee I.D',
        buttonNames: ['OK','Cancel']
      });
      dialog.addEventListener('click', function(e) {
        if(e.index === 0){ //ok
          checkIn(e.login,e.password,type).then(function(result){
            resolve(result);
          });
        }
      });
      dialog.show();
  });
}
// inital or logout setObject('auth',null)
// auth login correct setObject('auth',{BAPP:'Y',SA:'Y'}
// auth login incorrect setObject('auth',{BAPP:'N',SA:'N'}
function checkAuth(type){
  
  return new Promise((resolve,reject) => {
    let auth = Ti.App.Properties.getObject('auth');
    Ti.API.info('checkAuth=' + auth + ',type=' + type);
    if(auth !== null){
      Ti.API.info(auth[type])
      if(auth[type] === 'Y'){
        resolve({success:true});
      }else{
        resolve({success:false,msg:"You don't have right to access!"});
      }
    }else{
      resolve({success:false, msg:'The username and employee I.D are incorrect!'});
    }
  });
}

function checkIn(user,pwd,type){
  
  return new Promise((resolve,reject) => {
    Ti.API.info('checkIn');
    let list = loadingEmployeeData().results;
    let result = list.filter((item)=>{
        // Ti.API.info(item);
        return (item.name === user && item.employeeid === pwd)
      });
    if(result.length > 0){
      Ti.App.Properties.setObject('auth',{USER:user,ID:pwd,BAPP:result[0].rightofbapp,SA:result[0].rightofsa});
    }else{
      Ti.App.Properties.setObject('auth',null);
    }
    resolve(checkAuth(type));
  })
}

exports.checkOut = function(){
  Ti.App.Properties.removeProperty('auth');
}
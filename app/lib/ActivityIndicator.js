/*
 * Constructor
 */
var win = null;
var indicator = null;

var activityIndicator = function(){
	if( Ti.Platform.osname == "android" ){

		indicator = Ti.UI.Android.createProgressIndicator({
		  message: 'Loading...',
		  location: Ti.UI.Android.PROGRESS_INDICATOR_DIALOG,
		  cancelable: false
		});

	} else {
		win = Ti.UI.createWindow({
			backgroundColor: "#88000000",
			modal: Ti.Platform.osname == "android" ? true : false,
			navBarHidden: true
		});

		var container = Ti.UI.createView({
			height: Ti.UI.SIZE,
			width: Ti.UI.SIZE,
			backgroundColor: "#222",
			borderWidth: 1,
			borderRadius: 1,
			borderColor: "#444"
		})

		indicator = Ti.UI.createActivityIndicator({
			top: "16dp",
			bottom: "16dp",
			left: "16dp",
			right: "16dp",
			height: Ti.UI.SIZE,
			width: Ti.UI.SIZE,
			message: 'Loading...',
			color: "white"
		});

		// Prevent user from dismissing loading screen
		// if( Ti.Platform.osname == "android" ){ win.addEventListener("android:back", doNothing); }

		container.add(indicator);
		win.add(container);
	}
};
module.exports = activityIndicator;

/*
 * Show the Activity Indicator
 */
activityIndicator.prototype.start = function( msg ){
	if( msg ) { indicator.message = msg; }
	indicator.show();
	if( Ti.Platform.osname != "android" ) { win.open(); }
};

/*
 * Hide the Activity Indicator
 */
activityIndicator.prototype.stop = function(){
	indicator.hide();
	if( Ti.Platform.osname != "android" ) win.close();
};

/*
 * Change Indicator Message
 */
activityIndicator.prototype.setMessage = function( msg ){
	indicator.message = msg || "";
};

/*
 * Empty Function
 */
function doNothing(){ Ti.API.debug("Attempted to press back but button is temporarily disabled!"); };

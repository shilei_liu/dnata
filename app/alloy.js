// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};


/*
  Activity Indicator
*/
var ai = require("ActivityIndicator");
Alloy.Globals.ActivityIndicator = new ai();


/*
  Semantic Search
*/
let SemanticSearch = require('SemanticSearch');
Alloy.Globals.DNATA = new SemanticSearch('dnata');

/*
  Time and Date Display
  @description: Simple class for creating and formating dates uniformly
*/

var moment = require('/alloy/moment');
class ThisMoment {
  constructor( d = "", format = "DD-MM-YYYY" ) {
    if (d.length == 0) {
      this.now = moment();
    }
    else {
      this.now = moment(d, format);
    }
  }
  get dateString() {
    return this.now.format("DD/MM/YYYY");
  }
  get timeString() {
    return this.now.format("h:mm a");
  }
  get dateTimeString() {
    return this.now.format("Do MMM YYYY [at] h:mm a");
  }
}
Alloy.Globals.ThisMoment = ThisMoment;





/* 
	Dynamic Styling based off orientation
  @description: Code for detecting orientation changes and updating booleans for TSS 
*/

// Defaults
Alloy.Globals.isLandscape = false;
Alloy.Globals.isPortrait = true;

// Change if actually in Landscape
if ([Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT].indexOf(Ti.Gesture.orientation) > -1){
    Alloy.Globals.isLandscape = true;
    Alloy.Globals.isPortrait = false;
}

// Listen for changes
Ti.Gesture.addEventListener('orientationchange', (e) => {
	if ([Ti.UI.LANDSCAPE_LEFT, Ti.UI.LANDSCAPE_RIGHT].indexOf(e.orientation) > -1) {
    	Alloy.Globals.isLandscape = true;
    	Alloy.Globals.isPortrait = false;
  	} else {
    	Alloy.Globals.isLandscape = false;
    	Alloy.Globals.isPortrait = true;
  	}
});
/* Defaults */
var isSafe = false;
var isRisk = false;

var disabledBGColor = "#cacaca";
var safeEnabledBGColor = "#11CC11";
var riskEnabledBGColor = "#CC1111";
var midBGColor = "#1111CC";

/* Apply Styling */
$.container.applyProperties($.args);

/* Public Methods */
$.setColors = function(colors) {
	if(colors.disabledBGColor){ disabledBGColor = colors.disabledBGColor; }
	if(colors.safeEnabledBGColor){ safeEnabledBGColor = colors.safeEnabledBGColor; }
	if(colors.riskEnabledBGColor){ riskEnabledBGColor = colors.riskEnabledBGColor; }
	if(colors.midBGColor){ midBGColor = colors.midBGColor; }
}

$.getUIReference = function() {
	return {
		container: $.container,
		safe: $.safe,
		safeLabel: $.safeLabel,
		mid: $.mid,
		risk: $.risk,
		riskLabel: $.riskLabel,
	}
}

$.getSafeValue = function() {
	return isSafe;
}

$.getRiskValue = function() {
	return isRisk;
}

/* Private Methods */
function updateUI() {
	if ( isSafe && !isRisk ) {
		$.safe.backgroundColor = safeEnabledBGColor;
		$.mid.backgroundColor = disabledBGColor;
		$.risk.backgroundColor = disabledBGColor;
	}
	else if ( !isSafe && isRisk ) {
		$.safe.backgroundColor = disabledBGColor;
		$.mid.backgroundColor = disabledBGColor;
		$.risk.backgroundColor = riskEnabledBGColor;
	}
	else {
		$.safe.backgroundColor = disabledBGColor;
		$.mid.backgroundColor = midBGColor;
		$.risk.backgroundColor = disabledBGColor;
	}
}
updateUI();

/* Events */
function safeClicked() {
	isSafe = true;
	isRisk = false;
	updateUI();
}

function riskClicked() {
	isSafe = false;
	isRisk = true;
	updateUI();
}
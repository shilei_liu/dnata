/* Defaults */
var disabledBGColor = "#acacac";
var enabledBGColor = "#11CC11";

/* Apply Styling */
$.container.applyProperties($.args);

/* Public Methods */
$.getUIReference = function(){
	return {
		leftButton: $.subtract,
		rightButton: $.add,
		textField: $.number
	}
}

$.getValue = function() {
	return parseInt($.number.value);
}

/* Private Methods */
function updateUI() {
	var value = parseInt($.number.value);
	$.subtract.backgroundColor = value == 0 ? disabledBGColor : enabledBGColor;
}
updateUI();

/* Events */
function addOne() {
	$.number.value = parseInt($.number.value) + 1;
	updateUI();
}

function subtractOne() {
	var newValue = parseInt($.number.value) - 1;
	$.number.value = newValue < 0 ? 0 : newValue;
	updateUI();
}

function valueChanged(evt) {
	updateUI();
}

function cancel() {
	$.number.value = 0;
	$.number.blur();
}

function done() {
	$.number.blur();
}
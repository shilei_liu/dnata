/* Globals */
var pickerRows = [];
var selected = 0;

/* Public Methods */
$.init = function(args) {
	$.window.title = args.title || "";

	if (args.rows.length > 0) {
		pickerRows = args.rows.sort((a,b)=>{ return a.title > b.title; });
	}
	$.picker.add(pickerRows);
}

/* Private */
function selectedIndexForTitle(t) {
	for (var i=0, len=pickerRows.length; i<len; i++) {
		if ( pickerRows[i].title.includes(t) ) { return i;}
	}
	return -1;
}

function notifyChange(evt){
	selected = evt.rowIndex;
}

/* Incoming Events */
$.on('showSelected', (title) => {
	selected = selectedIndexForTitle(title);
	if (selected >= 0) {
		$.picker.setSelectedRow(0, selected, false);
	}
});

/* Outgoing Events */
function done() {
	$.trigger('pickerChange', pickerRows[selected]);
	$.popover.hide();
}

function cancel(evt) {
	$.trigger('pickerChange', {});
	$.popover.hide();
} 
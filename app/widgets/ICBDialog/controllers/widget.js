/* Globals */
let dataSource;
let categoryList = [];
let itemList = [];
let selected = "";
/* Public Methods */
$.init = function() {
 
  loadingData().then(result =>{
    Ti.API.info(result);
    let tmpObj = {};
    // format data
    result.map(item => {
      if(!tmpObj.hasOwnProperty(item.category)){
        tmpObj[item.category] = 1;
        categoryList.push({properties: { title: item.category }});
      }
    });

    return categoryList;
  }).then(categoryList => {
    //show the list

    $.ICBCategory.setItems(categoryList);
    $.ICBLVl.show();

  });
}

/* Private */
$.ICBLVl.addEventListener('itemclick', function (evt) {
  Ti.API.info(evt.itemIndex);
  let parent = categoryList[evt.itemIndex].properties.title;
  $.ICBItem.setItems(getChildren(parent));
  $.ICBLVr.show();
});

$.ICBLVr.addEventListener('itemclick', function(evt){
  selected = itemList[evt.itemIndex].properties.title;
  $.trigger('getICB',selected);
  Ti.API.info(selected);
});

function getChildren(parent){
  itemList = [];
  dataSource.filter(item => {
    return item.category == parent;
  }).map( child => {
    itemList.push({properties: { title: child.title}});
  });
  return itemList;
}

function loadingData(){
  return new Promise((resolve,reject) => {
    Alloy.Globals.DNATA.getICBData().then((result) => {
      dataSource = JSON.parse(result.response).results;
      resolve(dataSource);
    });  
  })
}

/* Incoming Events */


/* Outgoing Events */
function onConfirm() {
  Ti.API.info(selected);
  $.trigger('getICB',selected);
  $.popover.hide();
}

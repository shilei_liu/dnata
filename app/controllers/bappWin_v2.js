let args = $.args;
let bappList = [];
let navigation = $.args.navigation;

/* Setup Date */
let now = new Alloy.Globals.ThisMoment();
$.date.text = now.dateString;
$.time.text = now.timeString;

/* Setup Username and ID */
let auth = Ti.App.Properties.getObject('auth');
if(auth !== null){
  $.name.text = auth.USER;
  $.id.text = auth.ID;
}

loadingICBList();

/* Location */
let locationsData;
$.location.addEventListener("click", (evt) => {

  Ti.API.info(evt);
  Alloy.Globals.DNATA.getLocation().then((result) => {
    locationsData = JSON.parse(result.response).results
    Ti.API.info(locationsData)

    let locationOptions = [];
    let tmpObj = {};
    locationsData.forEach(item => {
      //unique value
      if(!tmpObj.hasOwnProperty(item.location)){
        tmpObj[item.location] = 1;
        locationOptions.push({title:item.location});
      }
    })

    // Create Popover
    let popover = Alloy.createWidget('popover-picker');
    popover.init({title: "Location", rows: locationOptions });

    // Handle Selection
    popover.on('pickerChange', (row) => { 
      $.location.value = row.title || "";
    });

    // Show Popover
    popover.getView().show({view: evt.source});
    if( $.location.value.length > 0 ){ 
    popover.trigger('showSelected', $.location.value);
  }

  });
});

/* Area */
$.area.addEventListener("click", (evt) => {

  let areaOptions = [];
  let tmpObj = {};
  if($.location.value.length === 0){
    alert('Pick the Location option first.');
    return;
  }
  
  locationsData.forEach(item => {
    if(item.location === $.location.value){
      //unique value
      if(!tmpObj.hasOwnProperty(item.area)){
        tmpObj[item.area] = 1;
        areaOptions.push({title:item.area});
      }
    }
  })

  // Create Popover
  let popover = Alloy.createWidget('popover-picker');
  popover.init({title: "Area", rows: areaOptions});

  // Handle Selection
  popover.on('pickerChange', (row) => { 
    $.area.value = row.title || "";
  });

  // Show Popover
  popover.getView().show({view: evt.source});
  if( $.area.value.length > 0 ){ 
    popover.trigger('showSelected', $.area.value);
  }

});

function showCamera (type, callback) {

    var camera = function () {
        // call Titanium.Media.showCamera and respond callbacks
        Ti.Media.showCamera({
            success: function (e) {
                callback(null, e);
            },
            cancel: function (e) {
                callback(e, null);
            },
            error: function (e) {
                callback(e, null);
            },
            saveToPhotoGallery: true, // save our media to the gallery
            mediaTypes: [ type ]
        });
    };
    // check if we already have permissions to capture media
    if (!Ti.Media.hasCameraPermissions()) {
      // request permissions to capture media
      Ti.Media.requestCameraPermissions(function (e) {
        // success! display the camera
        if (e.success) {
          camera();
        // oops! could not obtain required permissions
        } else {
          callback(new Error('could not obtain camera permissions!'), null);
        }
      });
    } else {
      camera();
    }
}

function loadingData(){
  return new Promise((resolve,reject) => {
    Alloy.Globals.DNATA.getICBData().then((result) => {
      dataSource = JSON.parse(result.response).results;
      resolve(dataSource);
    });  
  })
}

function onSwitch(){
   Ti.API.info('Switch value: ' + $.basicSwitch.value);
}

function onSAFE(evt){

  bappList[evt.itemIndex].ICBSAFE = {value: evt.value};
}
function onAtRisk(evt){
  
  bappList[evt.itemIndex].ICBAtRISK = {value: evt.value};
}

function onCamera(){
  showCamera(Ti.Media.MEDIA_TYPE_PHOTO, function (error, result) {
    if (error) {
      Ti.API.info(error);Ti.API.info(error);
      // alert('could not take photo');
      return;
    }
    // validate we taken a photo
    if (result.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
      $.cameraPic.image = result.media
    }
  });
}

function loadingICBList(){

  loadingData().then(result => {
    Ti.API.info(result)
    result.map(item => {
      Ti.API.info(item)
      let row = {};
      row.ICBCategory = {text:item.category};
      row.ICBTitle = {text:item.title};
      row.ICBSAFE = {value:false};
      row.ICBAtRISK = {value:false};
      row.template = 'template';
      bappList.push(row);
    });
    return bappList;
  }).then( bappList => {
    $.section.setItems(bappList);
    $.table.show();
  });
  
}

function resetICB(){
  $.basicSwitch.value = false;
  $.ICBBtn.value = '';
  $.ICBDesc.value = '';
  $.submitBtn.enabled = false;

}

function resetFields(){

  $.location.value = '';
  $.area.value = '';
  $.actTA.value = '';
  $.reviewSW.value = false;
  $.cameraPic.image = '/camera.png';

  resetICB();
  bappList = [];
  $.section.setItems(bappList);
  $.table.show();
}

function highlightField( field, message ) {
  var props = {borderColor: "#fd2c2c", borderRadius: 4, borderWidth: 2};
  field.applyProperties(props);
  
  var dialog = Ti.UI.createAlertDialog({
      message: message,
      ok: 'Ok',
      title: 'Error'
    });
    dialog.show();
}

function validate(){

  let details = bappList.reduce((arr,element) => {
    arr.push(
      {
        category:element.ICBCategory.text,
        title:element.ICBTitle.text,
        safe:element.ICBSAFE.value == 1,
        risk:element.ICBAtRISK.value == 1 
      })
    return arr;
  },[]);

  Ti.API.info(details);

  if( $.location.value.length == 0 ){
    highlightField($.location, "Select a location.");
    return false; 
  }

  if( $.area.value.length == 0 ){
    highlightField($.area, "Select a area or unit.");
    return false; 
  }

  email({type: "BAPP", content:{
    name: $.name.text,
    date: $.date.text,
    location: $.location.value,
    area: $.area.value,
    ctsg_value: $.ctsg.value ? "&check;" : "&nbsp;",
    ysg_value: $.ysg.value ? "&check;" : "&nbsp;",
    lgw_value: $.lgw.value ? "&check;" : "&nbsp;",
    safe_value: ($.basicSwitch.value == false) ? "&check;" : "&nbsp;",
    risk_value: ($.basicSwitch.value == true) ? "&check;" : "&nbsp;",
    what_happened: $.whatHappened.value,
    corrective_action: $.action.value,
    additional_notes: $.notes.value,
    discuss_value: $.discuss.value  ? "&check;" : "&nbsp;"
  }},{type:"CHECKLIST",content:details});
}

/* Send Email */
function emailErrorDialog(title, message){
  Ti.UI.createAlertDialog({
      message: message,
      ok: 'Ok',
      title: title
    }).show();
}

function email(valid,checklist) {
  // Start loading indicator
  Alloy.Globals.ActivityIndicator.start("Compiling Documents...");

    var HTMLReports = require('HTMLReports');
    var EmailAttachment = require('EmailAttachment');
    var files = {pdf: null, csv: null, checklist_pdf: null, checklist_csv: null};

    HTMLReports.generateReport(valid)
    .then( result => {
      return EmailAttachment.generatePDF(result.response, 'bapp.pdf');
    })
    .then( pdf => {
      // Ti.API.info('### pdf ###')
      Ti.API.info("BAPP PDF: "+pdf.file.path);
      files.pdf = pdf.file;
      return EmailAttachment.generateCSV({
        name: $.name.text,
        date: $.date.text,
        location: $.location.value,
        area: $.area.value,
        ctsg_value:$.ctsg.value,
        ysg_value:$.ysg.value,
        lgw_value:$.lgw.value,
        safe_value:$.basicSwitch.value == false,
        risk_value:$.basicSwitch.value == true,
        what_happened:$.whatHappened.value,
        corrective_action:$.action.value,
        additional_notes:$.notes.value,
        discuss_value:$.discuss.value
      }, 'bapp.csv');
    })
    .then( csv => {
      Ti.API.info("BAPP CSV: "+csv.file.path);
      files.csv = csv.file;

      return HTMLReports.generateReport(checklist)
      .then( resultList => {
        return EmailAttachment.generatePDF(resultList.response,'checklist.pdf');
      })
      .then( checklist_pdf => {
        Ti.API.info("CHESKLIST PDF: "+checklist_pdf.file.path);
        files.checklist_pdf = checklist_pdf.file;

        return EmailAttachment.generateCSV(checklist.content,'checklist.csv');
      })
      .then( checklist_csv => {
        Ti.API.info("CHESKLIST CSV: "+checklist_csv.file.path);
        files.checklist_csv = checklist_csv.file;

        var emailDialog = Ti.UI.createEmailDialog();
        emailDialog.subject = $.window.title;
        emailDialog.toRecipients = [];
        emailDialog.html=true;
        emailDialog.messageBody=`message:
          <br>
          <br>
          <b>content</b>
          </br>
        `;
        emailDialog.addAttachment(files.pdf);
        emailDialog.addAttachment(files.csv);
        emailDialog.addAttachment(files.checklist_pdf);
        emailDialog.addAttachment(files.checklist_csv);
        emailDialog.addEventListener('complete', evt => {
          // Stop Loading Indicator
          Alloy.Globals.ActivityIndicator.stop();
          
          // Do something based off result
          switch(evt.result){
            case emailDialog.SENT:
              navigation.closeWindow($.window);
              break;
            case emailDialog.SAVED:
              emailErrorDialog('Saved', 'The email has been saved and can be sent later from within the mail app.')
              break;
            case emailDialog.CANCELLED:
              emailErrorDialog('Cancelled', 'The email was cancelled successfully.')
              break;
            case emailDialog.FAILED:
              emailErrorDialog('Failed', 'The email failed to send. Please check your internet connection and try again.')
              break;
            default:
              emailErrorDialog('Error', 'There was an error when trying to send the email. Please try again.')
              break;
          }
        });
        emailDialog.open();
        // alert('emailDialog.open');
      })
      .catch( err => {
        // Stop loading indicator
        Alloy.Globals.ActivityIndicator.stop();
        
        // Handle errors
        Ti.API.error(err);
        Ti.UI.createAlertDialog({
          message: "The following error occurred:\n" + err.error,
          ok: 'Ok',
          title: (err.type || "Unknown").toUpperCase() + " Error"
        }).show();
      });
    })
    .catch( err => {
      // Stop loading indicator
      Alloy.Globals.ActivityIndicator.stop();
      
      // Handle errors
      Ti.API.error(err);
      Ti.UI.createAlertDialog({
        message: "The following error occurred:\n" + err.error,
        ok: 'Ok',
        title: (err.type || "Unknown").toUpperCase() + " Error"
      }).show();
    });
}
let args = $.args;
let bappList = [];

/* Setup Date */
let now = new Alloy.Globals.ThisMoment();
$.date.text = now.dateString;
$.time.text = now.timeString;

/* Setup Username and ID */
let auth = Ti.App.Properties.getObject('auth');
if(auth !== null){
  $.name.text = auth.USER;
  $.id.text = auth.ID;
}

/* Location */
let locationsData;
$.location.addEventListener("click", (evt) => {

  Ti.API.info(evt);
  Alloy.Globals.DNATA.getLocation().then((result) => {
    locationsData = JSON.parse(result.response).results
    Ti.API.info(locationsData)

    let locationOptions = [];
    let tmpObj = {};
    locationsData.forEach(item => {
      //unique value
      if(!tmpObj.hasOwnProperty(item.location)){
        tmpObj[item.location] = 1;
        locationOptions.push({title:item.location});
      }
    })

    // Create Popover
    let popover = Alloy.createWidget('popover-picker');
    popover.init({title: "Location", rows: locationOptions });

    // Handle Selection
    popover.on('pickerChange', (row) => { 
      $.location.value = row.title || "";
    });

    // Show Popover
    popover.getView().show({view: evt.source});
    if( $.location.value.length > 0 ){ 
    popover.trigger('showSelected', $.location.value);
  }

  });
});

/* Area */
$.area.addEventListener("click", (evt) => {

  let areaOptions = [];
  let tmpObj = {};
  if($.location.value.length === 0){
    alert('Pick the Location option first.');
    return;
  }
  
  locationsData.forEach(item => {
    if(item.location === $.location.value){
      //unique value
      if(!tmpObj.hasOwnProperty(item.area)){
        tmpObj[item.area] = 1;
        areaOptions.push({title:item.area});
      }
    }
  })

  // Create Popover
  let popover = Alloy.createWidget('popover-picker');
  popover.init({title: "Area", rows: areaOptions});

  // Handle Selection
  popover.on('pickerChange', (row) => { 
    $.area.value = row.title || "";
  });

  // Show Popover
  popover.getView().show({view: evt.source});
  if( $.area.value.length > 0 ){ 
    popover.trigger('showSelected', $.area.value);
  }

});

function showCamera (type, callback) {
    var camera = function () {
        // call Titanium.Media.showCamera and respond callbacks
        Ti.Media.showCamera({
            success: function (e) {
                callback(null, e);
            },
            cancel: function (e) {
                callback(e, null);
            },
            error: function (e) {
                callback(e, null);
            },
            saveToPhotoGallery: true, // save our media to the gallery
            mediaTypes: [ type ]
        });
    };
    // check if we already have permissions to capture media
    if (!Ti.Media.hasCameraPermissions()) {
      // request permissions to capture media
      Ti.Media.requestCameraPermissions(function (e) {
        // success! display the camera
        if (e.success) {
          camera();
        // oops! could not obtain required permissions
        } else {
          callback(new Error('could not obtain camera permissions!'), null);
        }
      });
    } else {
      camera();
    }
}

function onICB(){
  // Create Popover
  var popover = Alloy.createWidget('ICBDialog');
  popover.init();

  // Handle Selection
  popover.on('getICB', (title) => { 
    $.ICBBtn.value = title || "";
    if(title.length > 0){
      $.submitBtn.enabled = true;
    }
  });

  // Show Popover
  popover.getView().show({view: $.ICBBtn});
  
}

function onSwitch(){
   Ti.API.info('Switch value: ' + $.basicSwitch.value);

   if($.basicSwitch.value){
    $.ICBLbl.text = 'Workplace HAZARD/At-Risk';
   }else{
    $.ICBLbl.text = 'Workplace SAFE Behaviour/Person';
   }
}

function onCamera(){
  showCamera(Ti.Media.MEDIA_TYPE_PHOTO, function (error, result) {
    if (error) {
      Ti.API.info(error);Ti.API.info(error);
      // alert('could not take photo');
      return;
    }
    // validate we taken a photo
    if (result.mediaType == Ti.Media.MEDIA_TYPE_PHOTO) {
      $.cameraPic.image = result.media
    }
  });
}
function onDelete(evt){
  
  var dialog = Ti.UI.createAlertDialog({
    message: 'Do you want to delete this information?',
    buttonNames: ['Confirm', 'Cancel'],
    title: 'ICB Deleted'
  });
  dialog.addEventListener('click', function(e) {
    if (e.index === 0) {// confirm
      bappList.splice(evt.itemIndex,1);
      $.section.deleteItemsAt(evt.itemIndex,1);
    }
  });
  dialog.show();
}
function onSubmit(){

  let item = {};
  if($.basicSwitch.value === false){
    item.ICBstatus = {text:'SAFE',color:'green'};
  }else{
    item.ICBstatus = {text:'At-RISK',color:'red'};
  }
  item.icb = {text:$.ICBBtn.value};
  item.ICBComment = {text:$.ICBDesc.value};
  item.template = 'template';
  bappList.push(item);
  
  $.section.setItems(bappList);
  $.table.show();
  resetICB();
}

function resetICB(){
  $.basicSwitch.value = false;
  $.ICBBtn.value = '';
  $.ICBDesc.value = '';
  $.submitBtn.enabled = false;

}

function resetFields(){

  $.location.value = '';
  $.area.value = '';
  $.actTA.value = '';
  $.reviewSW.value = false;
  $.cameraPic.image = '/camera.png';

  resetICB();
  bappList = [];
  $.section.setItems(bappList);
  $.table.show();
}

function highlightField( field, message ) {
  var props = {borderColor: "#fd2c2c", borderRadius: 4, borderWidth: 2};
  field.applyProperties(props);
  
  var dialog = Ti.UI.createAlertDialog({
      message: message,
      ok: 'Ok',
      title: 'Error'
    });
    dialog.show();
}

function validate(){
  //resetFields();

  if( $.location.value.length == 0 ){
    highlightField($.location, "Select a location.");
    return false; 
  }

  if( $.area.value.length == 0 ){
    highlightField($.area, "Select a area or unit.");
    return false; 
  }

  if( $.ICBBtn.value.length == 0 ){
    highlightField($.ICBBtn, "Select an ICB.");
    return false; 
  }

  email({type: "BAPP", content:{
    name: $.name.value,
    id: $.id.text,
    num_observed: $.people.getValue(),
    date: $.date.text,
    location: $.location.value,
    area: $.area.value
  }});
}

/* Send Email */

function email(valid) {
  // Start loading indicator
  Alloy.Globals.ActivityIndicator.start("Compiling Documents...");

    var HTMLReports = require('HTMLReports');
    var EmailAttachment = require('EmailAttachment');
    var files = {pdf: null, csv: null};

    HTMLReports.generateReport(valid)
    .then( result => {
      return EmailAttachment.generatePDF(result.response);
    })
    .then( pdf => {
      Ti.API.info(pdf.file);
      files.pdf = pdf.file;
      return EmailAttachment.generateCSV({
        date: valid.content.date,
        employee_name: valid.content.name,
      employee_id: valid.content.id,
      location: valid.content.location,
      area: valid.content.area


      });
    })
    .then( csv => {
      Ti.API.info(csv.file);
      files.csv = csv.file;

      var emailDialog = Ti.UI.createEmailDialog();
      emailDialog.subject = $.window.title;
      emailDialog.toRecipients = [];
      emailDialog.html=true;
      emailDialog.messageBody=`message:
        <br>
        <br>
        <b>content</b>
        </br>
      `;
    emailDialog.addAttachment(files.pdf);
    emailDialog.addAttachment(files.csv);
    emailDialog.addEventListener('complete', evt => {
      // Stop Loading Indicator
      Alloy.Globals.ActivityIndicator.stop();
      
      // Do something based off result
      switch(evt.result){
        case emailDialog.SENT:
          navigation.closeWindow($.window);
          break;
        case emailDialog.SAVED:
          emailErrorDialog('Saved', 'The email has been saved and can be sent later from within the mail app.')
          break;
        case emailDialog.CANCELLED:
          emailErrorDialog('Cancelled', 'The email was cancelled successfully.')
          break;
        case emailDialog.FAILED:
          emailErrorDialog('Failed', 'The email failed to send. Please check your internet connection and try again.')
          break;
        default:
          emailErrorDialog('Error', 'There was an error when trying to send the email. Please try again.')
          break;
      }
    });

      // emailDialog.open();
      alert('emailDialog.open');
    })
    .catch( err => {
      // Stop loading indicator
      Alloy.Globals.ActivityIndicator.stop();
      
      // Handle errors
      Ti.API.error(err);
      Ti.UI.createAlertDialog({
        message: "The following error occurred:\n" + err.error,
        ok: 'Ok',
        title: err.type.toUpperCase() + " Error"
      }).show();
    });
}
let Authentication = require('Authentication');
let employeeData;


// inital employee data
function inital(){
  employeeData = Authentication.loadingEmployeeData();
  // Ti.API.info(employeeData)
}

function onBAPP(){
  
  Authentication.login('BAPP')
    .then((result) => {
      if(result.success){
        let bappWin = Alloy.createController('bappWin_v2',{navigation: $.nav}).getView();
        $.nav.openWindow(bappWin);  
      }else{
        alert(result.msg);
      }
    })
}

function onSafety() {
  Authentication.login('SA')
    .then((result) => {
      if(result.success){
        var safetyScreen = Alloy.createController('safetyScreen',{navigation: $.nav}).getView();
        $.nav.openWindow(safetyScreen);  
      }else{
        alert(result.msg);
      }
    })
}

function onLogout(){
  Authentication.checkOut();
}
inital();
$.nav.open();
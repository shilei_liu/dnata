// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var navigation = $.args.navigation;
var user = Ti.App.Properties.getObject('auth');

var dnata = {
	// {title: "Rory Best", id: "ULS0002"},
	employees: JSON.parse( Ti.App.Properties.getString('picker_employee_list', '[]') ),
	// {location: "Ravenhill", department: "Coaching"}
	locations: JSON.parse( Ti.App.Properties.getString('picker_location_list', '[]') ),
	getUniqueLocations: function() {
		// Get unique locations, then format as [{title: "Ravenhill"}]
		let unique = [...new Set(this.locations.map(l => l.location ))];
		return unique.map(val => { return {title: val} });
	},
	getDepartmentsAt: function(selectedLocation) {
		// Filter by location, then format as [{title: "Coaching"}]
		let filtered = this.locations.filter(l => { return l.location == selectedLocation; });
		return filtered.map(l => { return {title: l.department} });
	}
}

/* Setup Date */
var now = new Alloy.Globals.ThisMoment();
$.date.text = now.dateString;

/* Setup Pickers */


function formatData(){

	// Employees
	Alloy.Globals.DNATA.getEmployeeData()
	.then(result => {
		// Get the object
		let employees = JSON.parse(result.response).results;
		
		// Format for Picker List
		let formatted = employees.map(emp => {
			let name = emp.name.toLowerCase();
			let nameCased = name.replace(/\b(\w)/g, s => s.toUpperCase());
			return {title: nameCased, id: emp.employeeid}
		});

		// Save in storage for easy access later
		Ti.App.Properties.setString('picker_employee_list', JSON.stringify(formatted));

		// Set the value in the dnata variable
		dnata.employees = formatted;
	})
	.catch(err => {
		Ti.API.error(`${err.code}: ${err.error}`);
	});

	// Locations & Departments
	Alloy.Globals.DNATA.getLocation()
	.then(result => {
		let locations = JSON.parse(result.response).results;

		let formatted = locations.map(loc => {
			let location = loc.location.toLowerCase();
			let locationCased = location.replace(/\b(\w)/g, s => s.toUpperCase());
			
			let dept = loc.area.toLowerCase();
			let deptCased = dept.replace(/\b(\w)/g, s => s.toUpperCase());
			
			return {location: locationCased, department: deptCased}
		});

		Ti.App.Properties.setString('picker_location_list', JSON.stringify(formatted));
		dnata.locations = formatted;
	})
	.catch(err => {
		Ti.API.error(`${err.code}: ${err.error}`);
	});
}
formatData();

$.name.addEventListener("click", (evt) => {
	// Create Popover
	var popover = Alloy.createWidget('popover-picker');
	popover.init({title: "Name", rows: dnata.employees });

	// Handle Selection
	popover.on('pickerChange', (row) => { 
		$.name.value = row.title || "";
		$.id.text = row.id || "";
	});

	// Show Popover
	popover.getView().show({view: evt.source});
	if( $.name.value.length > 0 ){ 
		popover.trigger('showSelected', $.name.value);
	}
});

$.location.addEventListener("click", (evt) => {
	// Create Popover
	var popover = Alloy.createWidget('popover-picker');
	popover.init({title: "Location", rows: dnata.getUniqueLocations() });

	// Handle Selection
	popover.on('pickerChange', (row) => { 
		$.location.value = row.title || "";
	});

	// Show Popover
	popover.getView().show({view: evt.source});
	if( $.location.value.length > 0 ){ 
		popover.trigger('showSelected', $.location.value);
	}
});

$.department.addEventListener("click", (evt) => {

	// Make sure there is a location for the department
	if( $.location.value.length == 0 ){
		highlightField($.location, "Please select a location first.");
	 	return false; 
	}

	// Create Popover
	var popover = Alloy.createWidget('popover-picker');
	popover.init({title: "Dept.", rows: dnata.getDepartmentsAt($.location.value) });

	// Handle Selection
	popover.on('pickerChange', (row) => { 
		$.department.value = row.title || "";
	});

	// Show Popover
	popover.getView().show({view: evt.source});
	if( $.department.value.length > 0 ){ 
		popover.trigger('showSelected', $.department.value);
	}
});

/* Setup Stepper */
var stepperComponents = $.people.getUIReference();

/* Setup Trinary */
var trinaryComponents = $.safe.getUIReference();

/* Data Validation */

function validate() {
	resetFields();

	if( $.name.value.length == 0 ){
		highlightField($.name, "Select a person by name.");
	 	return false; 
	}

	if( $.people.getValue() == 0 ){
		highlightField(stepperComponents.textField, "Please enter the number of people observed (greater than 0)."); 
		return false; 
	}

	if( $.location.value.length == 0 ){
		highlightField($.location, "Select a location.");
	 	return false; 
	}

	if( $.department.value.length == 0 ){
		highlightField($.department, "Select a department or unit.");
	 	return false; 
	}

	if( !($.safe.getSafeValue() ^ $.safe.getRiskValue()) ) {
		highlightField(trinaryComponents.container, "Please choose either SAFE or RISK.");
		return false;
	}

	if( $.action_observed.value.length == 0 ){
		highlightField($.action_observed, "Fill in the action that was observed.");
	 	return false; 
	}

	if( $.safe.getRiskValue() ) {
		if( $.consequences.value.length == 0 ){ 
			highlightField($.consequences, "Fill in the potential consequences of the observed action.");
			return false;
		}
	}

	if( $.action_taken.value.length == 0 ){
		highlightField($.action_taken, "Fill in the action taken in response to the observation.");
	 	return false; 
	}

	email({type: "SAFETY", content:{
		name: $.name.value,
		id: $.id.text,
		num_observed: $.people.getValue(),
		date: $.date.text,
		location: $.location.value,
		department: $.department.value,
		safe: $.safe.getSafeValue(),
		safe_value: $.safe.getSafeValue() ? "&check;" : "&nbsp;",
		risk: $.safe.getRiskValue(),
		risk_value: $.safe.getRiskValue() ? "&check;" : "&nbsp;",
		action_observed: $.action_observed.value,
		consequences: $.consequences.value,
		action_taken: $.action_taken.value
	}});
}

function highlightField( field, message ) {
	var props = {borderColor: "#fd2c2c", borderRadius: 4, borderWidth: 2};
	field.applyProperties(props);
	
	var dialog = Ti.UI.createAlertDialog({
    	message: message,
    	ok: 'Ok',
    	title: 'Error'
  	});
  	dialog.show();
}

function resetFields() {
	var props = {borderColor: "#bbbbbb", borderRadius: 4, borderWidth: 1};
	var fields = [$.name, $.location, $.department, $.action_observed, $.consequences, $.action_taken, stepperComponents.textField, trinaryComponents.container];
	fields.forEach(f => { f.applyProperties(props)});
}

/* Send Email */

function email(valid) {
	// Start loading indicator
	Alloy.Globals.ActivityIndicator.start("Compiling Documents...");

  	var HTMLReports = require('HTMLReports');
  	var EmailAttachment = require('EmailAttachment');
  	var files = {pdf: null, csv: null};

  	HTMLReports.generateReport(valid)
  	.then( result => {
  		return EmailAttachment.generatePDF(result.response, 'safety.pdf');
  	})
  	.then( pdf => {
  		Ti.API.info(pdf.file);
  		files.pdf = pdf.file;
  		return EmailAttachment.generateCSV({
  			date: valid.content.date,
  			employee_name: valid.content.name,
			employee_id: valid.content.id,
			number_of_people_observed: valid.content.num_observed,
			location: valid.content.location,
			department: valid.content.department,
			action_observed: valid.content.action_observed,
			potential_consequences: valid.content.consequences,
			action_taken: valid.content.action_taken,
			behaviour: valid.content.safe ? "Safe" : "Risk"
  		}, 'safety.csv');
  	})
  	.then( csv => {
  		Ti.API.info(csv.file);
  		files.csv = csv.file;

  		var emailDialog = Ti.UI.createEmailDialog();
    	emailDialog.subject = $.window.title;
    	emailDialog.toRecipients = [];
    	emailDialog.html=true;
    	emailDialog.messageBody=`message:
    		<br>
    		<br>
    		<b>content</b>
    		</br>
    	`;
		emailDialog.addAttachment(files.pdf);
		emailDialog.addAttachment(files.csv);
		emailDialog.addEventListener('complete', evt => {
			// Stop Loading Indicator
			Alloy.Globals.ActivityIndicator.stop();
			
			// Do something based off result
			switch(evt.result){
				case emailDialog.SENT:
					navigation.closeWindow($.window);
					break;
				case emailDialog.SAVED:
					emailErrorDialog('Saved', 'The email has been saved and can be sent later from within the mail app.')
					break;
				case emailDialog.CANCELLED:
					emailErrorDialog('Cancelled', 'The email was cancelled successfully.')
					break;
				case emailDialog.FAILED:
					emailErrorDialog('Failed', 'The email failed to send. Please check your internet connection and try again.')
					break;
				default:
					emailErrorDialog('Error', 'There was an error when trying to send the email. Please try again.')
					break;
			}
		});

    	emailDialog.open();
  	})
  	.catch( err => {
  		// Stop loading indicator
  		Alloy.Globals.ActivityIndicator.stop();
  		
  		// Handle errors
  		Ti.API.error(err);
  		Ti.UI.createAlertDialog({
	    	message: "The following error occurred:\n" + err.error,
	    	ok: 'Ok',
	    	title: err.type.toUpperCase() + " Error"
	  	}).show();
  	});
}

function emailErrorDialog(title, message){
	Ti.UI.createAlertDialog({
    	message: message,
    	ok: 'Ok',
    	title: title
  	}).show();
}

/* Temporary Storage */